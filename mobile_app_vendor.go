package google_ads

import "google.golang.org/genproto/googleapis/ads/googleads/v18/enums"

type MobileAppVendor = enums.MobileAppVendorEnum_MobileAppVendor

const (
	// AppStoreUnspecified Not specified.
	AppStoreUnspecified MobileAppVendor = enums.MobileAppVendorEnum_UNSPECIFIED
	// AppStoreUnknown Used for return value only. Represents value unknown in this version.
	AppStoreUnknown MobileAppVendor = enums.MobileAppVendorEnum_UNKNOWN
	// AppStoreApple Apple iTunes.
	AppStoreApple MobileAppVendor = enums.MobileAppVendorEnum_APPLE_APP_STORE
	// AppStoreGoogle Google Play.
	AppStoreGoogle MobileAppVendor = enums.MobileAppVendorEnum_GOOGLE_APP_STORE
)
