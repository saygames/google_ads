package google_ads

import (
	"path"
)

func ResourceId(resourcePath string) string {
	_, id := path.Split(resourcePath)
	return id
}
