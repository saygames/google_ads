package google_ads

import (
	"log"
	"time"
)

// SunsetDate https://developers.google.com/google-ads/api/docs/sunset-dates
var SunsetDate = time.Date(2025, time.September, 24, 0, 0, 0, 0, time.UTC)

func Version() string {
	return version
}

const (
	version      = "v18"
	notifySunset = 60 * 24 * time.Hour
)

func init() {
	notify := SunsetDate.Add(-notifySunset)
	if time.Now().After(notify) {
		log.Printf("WARNING: google_ads_api %s sunset date is %s. Please upgrade https://developers.google.com/google-ads/api/docs/sunset-dates", version, SunsetDate.Format("2006-01-02"))
	}
}
