package google_ads

import "google.golang.org/genproto/googleapis/ads/googleads/v18/resources"

type CampaignBudget = resources.CampaignBudget

func CampaignBudgetResourceName(customerId string, budgetId string) string {
	return "customers/" + customerId + "/campaignBudgets/" + budgetId
}
