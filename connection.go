package google_ads

import (
	"context"
	"slices"
	"strings"

	"github.com/TelpeNight/oauthctx"
	grpcctx "github.com/TelpeNight/oauthctx/grpc"
	"golang.org/x/oauth2"
	gauth "golang.org/x/oauth2/google"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	gcred "google.golang.org/grpc/credentials/google"
)

type Connection interface {
	grpc.ClientConnInterface
	GetCustomerId(ctx context.Context) (string, error)
	Close() error
}

func NewConnection(b credentials.Bundle, opts ...grpc.DialOption) (Connection, error) {
	// bundle is strictly required, and is added to opts as last (higher priority) value
	opts = append(slices.Clone(opts), grpc.WithCredentialsBundle(b))
	d, e := grpc.NewClient("googleads.googleapis.com:443", opts...)
	if e != nil {
		return nil, e
	}
	return &auth{d, asCustomerId(b.PerRPCCredentials())}, nil
}

type OAuthConfig struct {
	// ClientID is the application's ID.
	ClientID string

	// ClientSecret is the application's secret.
	ClientSecret string

	// RefreshToken is a token that's used by the application
	// (as opposed to the user) to refresh the access token
	// if it expires.
	RefreshToken string

	//A developer token is a 22-character string that uniquely identifies a Google
	//Ads API developer. An example developer token string is
	//ABcdeFGH93KL-NOPQ_STUv. The developer token should be included in the form of
	//developer-token : ABcdeFGH93KL-NOPQ_STUv.
	DeveloperToken string

	//This is the customer ID of the authorized customer to use in the request,
	//without hyphens (-). If your access to the customer account is through a
	//manager account, this header is required and must be set to the customer ID of
	//the manager account.
	LoginCustomerID string
}

func (c OAuthConfig) TokenSource() oauthctx.TokenSource {
	return oauthctx.ReuseTokenSource(nil, c.tokenSource())
}

func (c OAuthConfig) Bundle() credentials.Bundle {
	c.LoginCustomerID = c.NormalizedLoginCustomerID()
	return gcred.NewDefaultCredentialsWithOptions(
		gcred.DefaultCredentialsOptions{
			PerRPCCreds: &bundlePerRPCCredentials{
				TokenSource: &grpcctx.TokenSource{
					TokenSource: c.TokenSource(),
				},
				config: c,
			},
		},
	)
}

type OAuthConfigSource interface {
	GetOAuthConfig(ctx context.Context) (OAuthConfig, error)
}

func NewOAuthConfigBundle(src OAuthConfigSource) credentials.Bundle {
	creds := &oauthConfigPerRPCCredentials{new: src, mu: make(chan struct{}, 1)}
	creds.mu <- struct{}{}
	// creds also implements oauthctx.TokenSource
	// when token is not valid, TokenContext method will be called
	// this will also refresh MD
	creds.grpc = grpcctx.TokenSource{
		TokenSource: oauthctx.ReuseTokenSource(nil, creds),
	}
	return gcred.NewDefaultCredentialsWithOptions(
		gcred.DefaultCredentialsOptions{
			PerRPCCreds: creds,
		},
	)
}

func (c OAuthConfig) tokenSource() oauthctx.TokenSource {
	conf := oauthctx.NewConfig(&oauth2.Config{
		ClientID:     c.ClientID,
		ClientSecret: c.ClientSecret,
		Endpoint:     gauth.Endpoint,
	})
	return conf.TokenSource(&oauth2.Token{RefreshToken: c.RefreshToken})
}

func (c OAuthConfig) mdOfNormalized(mp map[string]string) map[string]string {
	if mp == nil {
		mp = make(map[string]string, 2)
	}
	mp["developer-token"] = c.DeveloperToken
	mp[loginCustomerIdMDKey] = c.LoginCustomerID
	return mp
}

func (c OAuthConfig) NormalizedLoginCustomerID() string {
	return strings.ReplaceAll(c.LoginCustomerID, "-", "")
}

func asCustomerId(creds credentials.PerRPCCredentials) customerId {
	// by google documentation, credentials.PerRPCCredentials should provide customer id
	// no default grpc implementation does it, so we expect a cred from this pkg to implement customerId
	if c, is := creds.(customerId); is {
		return c
	}
	// if not, try to use GetRequestMetadata and extract the id
	// but in general it won't work, 'cause of grpc inner checks
	return &metadataCustomerId{creds}
}

type auth struct {
	*grpc.ClientConn
	customerId
}

type customerId interface {
	GetCustomerId(ctx context.Context) (string, error)
}

type metadataCustomerId struct {
	creds credentials.PerRPCCredentials
}

const loginCustomerIdMDKey = "login-customer-id"

func (m *metadataCustomerId) GetCustomerId(ctx context.Context) (string, error) {
	md, err := m.creds.GetRequestMetadata(ctx)
	if err != nil {
		return "", err
	}
	return md[loginCustomerIdMDKey], nil
}

type bundlePerRPCCredentials struct {
	*grpcctx.TokenSource
	config OAuthConfig // with normalized customer id
}

var _ customerId = (*bundlePerRPCCredentials)(nil)

func (p *bundlePerRPCCredentials) GetRequestMetadata(ctx context.Context, uri ...string) (map[string]string, error) {
	md, err := p.TokenSource.GetRequestMetadata(ctx, uri...)
	if err != nil {
		return nil, err
	}
	return p.config.mdOfNormalized(md), nil
}

func (p *bundlePerRPCCredentials) GetCustomerId(context.Context) (string, error) {
	return p.config.LoginCustomerID, nil
}

type oauthConfigPerRPCCredentials struct {
	new   OAuthConfigSource
	grpc  grpcctx.TokenSource
	mu    chan struct{} // guards creds and src
	creds *OAuthConfig
	src   oauthctx.TokenSource
}

var _ customerId = (*oauthConfigPerRPCCredentials)(nil)

func (l *oauthConfigPerRPCCredentials) GetRequestMetadata(ctx context.Context, uri ...string) (map[string]string, error) {
	md, err := l.grpc.GetRequestMetadata(ctx, uri...)
	if err != nil {
		return nil, err
	}
	return l.md(ctx, md)
}

func (l *oauthConfigPerRPCCredentials) TokenContext(ctx context.Context) (*oauth2.Token, error) {
	err := l.lock(ctx)
	if err != nil {
		return nil, err
	}
	defer l.unlock()

	err = l.refreshLocked(ctx)
	if err != nil {
		return nil, err
	}

	return l.src.TokenContext(ctx)
}

func (l *oauthConfigPerRPCCredentials) RequireTransportSecurity() bool {
	return l.grpc.RequireTransportSecurity()
}

func (l *oauthConfigPerRPCCredentials) GetCustomerId(ctx context.Context) (string, error) {
	// refresh md if token was expired
	_, err := l.grpc.TokenContext(ctx)
	if err != nil {
		return "", err
	}

	err = l.lock(ctx)
	if err != nil {
		return "", err
	}
	defer l.unlock()

	if l.creds == nil { // just in case...
		err := l.refreshLocked(ctx)
		if err != nil {
			return "", err
		}
	}

	return l.creds.LoginCustomerID, nil
}

func (l *oauthConfigPerRPCCredentials) md(ctx context.Context, mp map[string]string) (map[string]string, error) {
	err := l.lock(ctx)
	if err != nil {
		return nil, err
	}
	defer l.unlock()

	if l.creds == nil {
		err := l.refreshLocked(ctx)
		if err != nil {
			return nil, err
		}
	}
	return l.creds.mdOfNormalized(mp), nil
}

func (l *oauthConfigPerRPCCredentials) refreshLocked(ctx context.Context) error {
	creds, err := l.new.GetOAuthConfig(ctx)
	if err != nil {
		return err
	}
	creds.LoginCustomerID = creds.NormalizedLoginCustomerID()
	if l.creds == nil || *l.creds != creds {
		l.creds = &creds
		l.src = l.creds.tokenSource()
	}
	return nil
}

func (l *oauthConfigPerRPCCredentials) lock(ctx context.Context) error {
	select {
	case <-ctx.Done():
		return ctx.Err()
	default:
	}
	select {
	case <-ctx.Done():
		return ctx.Err()
	case <-l.mu:
		return nil
	}
}

func (l *oauthConfigPerRPCCredentials) unlock() {
	l.mu <- struct{}{}
}
