package google_ads

import "google.golang.org/genproto/googleapis/ads/googleads/v18/enums"

type ChangeResourceType = enums.ChangeEventResourceTypeEnum_ChangeEventResourceType

const (
	// ChangeResourceTypeUNSPECIFIED No value has been specified.
	ChangeResourceTypeUNSPECIFIED ChangeResourceType = enums.ChangeEventResourceTypeEnum_UNSPECIFIED
	// ChangeResourceTypeUNKNOWN Used for return value only. Represents an unclassified resource unknown
	// in this version.
	ChangeResourceTypeUNKNOWN ChangeResourceType = enums.ChangeEventResourceTypeEnum_UNKNOWN
	// ChangeResourceTypeAd An Ad resource change.
	ChangeResourceTypeAd ChangeResourceType = enums.ChangeEventResourceTypeEnum_AD
	// ChangeResourceTypeAdGroup An AdGroup resource change.
	ChangeResourceTypeAdGroup ChangeResourceType = enums.ChangeEventResourceTypeEnum_AD_GROUP
	// ChangeResourceTypeAdGroupCriterion An AdGroupCriterion resource change.
	ChangeResourceTypeAdGroupCriterion ChangeResourceType = enums.ChangeEventResourceTypeEnum_AD_GROUP_CRITERION
	// ChangeResourceTypeCampaign A Campaign resource change.
	ChangeResourceTypeCampaign ChangeResourceType = enums.ChangeEventResourceTypeEnum_CAMPAIGN
	// ChangeResourceTypeCampaignBudget A CampaignBudget resource change.
	ChangeResourceTypeCampaignBudget ChangeResourceType = enums.ChangeEventResourceTypeEnum_CAMPAIGN_BUDGET
	// ChangeResourceTypeAdGroupBidModifier An AdGroupBidModifier resource change.
	ChangeResourceTypeAdGroupBidModifier ChangeResourceType = enums.ChangeEventResourceTypeEnum_AD_GROUP_BID_MODIFIER
	// ChangeResourceTypeCampaignCriterion A CampaignCriterion resource change.
	ChangeResourceTypeCampaignCriterion ChangeResourceType = enums.ChangeEventResourceTypeEnum_CAMPAIGN_CRITERION
	// ChangeResourceTypeFeed A Feed resource change.
	ChangeResourceTypeFeed ChangeResourceType = enums.ChangeEventResourceTypeEnum_FEED
	// ChangeResourceTypeFeedItem A FeedItem resource change.
	ChangeResourceTypeFeedItem ChangeResourceType = enums.ChangeEventResourceTypeEnum_FEED_ITEM
	// ChangeResourceTypeCampaignFeed A CampaignFeed resource change.
	ChangeResourceTypeCampaignFeed ChangeResourceType = enums.ChangeEventResourceTypeEnum_CAMPAIGN_FEED
	// ChangeResourceTypeAAdGroupFeed An AdGroupFeed resource change.
	ChangeResourceTypeAAdGroupFeed ChangeResourceType = enums.ChangeEventResourceTypeEnum_AD_GROUP_FEED
	// ChangeResourceTypeAdGroupAd An AdGroupAd resource change.
	ChangeResourceTypeAdGroupAd ChangeResourceType = enums.ChangeEventResourceTypeEnum_AD_GROUP_AD
	// ChangeResourceTypeAsset An Asset resource change.
	ChangeResourceTypeAsset ChangeResourceType = enums.ChangeEventResourceTypeEnum_ASSET
	// ChangeResourceTypeCustomerAsset A CustomerAsset resource change.
	ChangeResourceTypeCustomerAsset ChangeResourceType = enums.ChangeEventResourceTypeEnum_CUSTOMER_ASSET
	// ChangeResourceTypeCampaignAsset A CampaignAsset resource change.
	ChangeResourceTypeCampaignAsset ChangeResourceType = enums.ChangeEventResourceTypeEnum_CAMPAIGN_ASSET
	// ChangeResourceTypeAdGroupAsset An AdGroupAsset resource change.
	ChangeResourceTypeAdGroupAsset ChangeResourceType = enums.ChangeEventResourceTypeEnum_AD_GROUP_ASSET
	// ChangeResourceTypeAssetSet An AssetSet resource change.
	ChangeResourceTypeAssetSet ChangeResourceType = enums.ChangeEventResourceTypeEnum_ASSET_SET
	// ChangeResourceTypeAssetSetAsset An AssetSetAsset resource change.
	ChangeResourceTypeAssetSetAsset ChangeResourceType = enums.ChangeEventResourceTypeEnum_ASSET_SET_ASSET
	// ChangeResourceTypeCampaignAssetSet A CampaignAssetSet resource change.
	ChangeResourceTypeCampaignAssetSet ChangeResourceType = enums.ChangeEventResourceTypeEnum_CAMPAIGN_ASSET_SET
)
