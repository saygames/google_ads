package google_ads

import (
	"context"
	"errors"
	"strings"

	"google.golang.org/genproto/googleapis/ads/googleads/v18/enums"
	"google.golang.org/genproto/googleapis/ads/googleads/v18/services"
	"google.golang.org/grpc"
	"google.golang.org/protobuf/types/known/fieldmaskpb"
)

type GoogleAdsService interface {
	// SearchStream returns all rows that match the search stream query.
	//
	// List of thrown errors:
	//   [AuthenticationError]()
	//   [AuthorizationError]()
	//   [ChangeEventError]()
	//   [ChangeStatusError]()
	//   [ClickViewError]()
	//   [HeaderError]()
	//   [InternalError]()
	//   [QueryError]()
	//   [QuotaError]()
	//   [RequestError]()
	SearchStream(ctx context.Context, in *SearchStreamRequest, opts ...grpc.CallOption) (SearchStream, error)

	// Mutate creates, updates, or removes resources. This method supports atomic
	// transactions with multiple types of resources. For example, you can
	// atomically create a campaign and a campaign budget, or perform up to
	// thousands of mutates atomically.
	//
	// This method is essentially a wrapper around a series of mutate methods. The
	// only features it offers over calling those methods directly are:
	//
	// - Atomic transactions
	// - Temp resource names (described below)
	// - Somewhat reduced latency over making a series of mutate calls
	//
	// Note: Only resources that support atomic transactions are included, so this
	// method can't replace all calls to individual services.
	//
	// ## Atomic Transaction Benefits
	//
	// Atomicity makes error handling much easier. If you're making a series of
	// changes and one fails, it can leave your account in an inconsistent state.
	// With atomicity, you either reach the chosen state directly, or the request
	// fails and you can retry.
	//
	// ## Temp Resource Names
	//
	// Temp resource names are a special type of resource name used to create a
	// resource and reference that resource in the same request. For example, if a
	// campaign budget is created with `resource_name` equal to
	// `customers/123/campaignBudgets/-1`, that resource name can be reused in
	// the `Campaign.budget` field in the same request. That way, the two
	// resources are created and linked atomically.
	//
	// To create a temp resource name, put a negative number in the part of the
	// name that the server would normally allocate.
	//
	// Note:
	//
	// - Resources must be created with a temp name before the name can be reused.
	//   For example, the previous CampaignBudget+Campaign example would fail if
	//   the mutate order was reversed.
	// - Temp names are not remembered across requests.
	// - There's no limit to the number of temp names in a request.
	// - Each temp name must use a unique negative number, even if the resource
	//   types differ.
	//
	// ## Latency
	//
	// It's important to group mutates by resource type or the request may time
	// out and fail. Latency is roughly equal to a series of calls to individual
	// mutate methods, where each change in resource type is a new call. For
	// example, mutating 10 campaigns then 10 ad groups is like 2 calls, while
	// mutating 1 campaign, 1 ad group, 1 campaign, 1 ad group is like 4 calls.
	//
	// List of thrown errors:
	//   [AdCustomizerError]()
	//   [AdError]()
	//   [AdGroupAdError]()
	//   [AdGroupCriterionError]()
	//   [AdGroupError]()
	//   [AssetError]()
	//   [AuthenticationError]()
	//   [AuthorizationError]()
	//   [BiddingError]()
	//   [CampaignBudgetError]()
	//   [CampaignCriterionError]()
	//   [CampaignError]()
	//   [CampaignExperimentError]()
	//   [CampaignSharedSetError]()
	//   [CollectionSizeError]()
	//   [ContextError]()
	//   [ConversionActionError]()
	//   [CriterionError]()
	//   [CustomerFeedError]()
	//   [DatabaseError]()
	//   [DateError]()
	//   [DateRangeError]()
	//   [DistinctError]()
	//   [ExtensionFeedItemError]()
	//   [ExtensionSettingError]()
	//   [FeedAttributeReferenceError]()
	//   [FeedError]()
	//   [FeedItemError]()
	//   [FeedItemSetError]()
	//   [FieldError]()
	//   [FieldMaskError]()
	//   [FunctionParsingError]()
	//   [HeaderError]()
	//   [ImageError]()
	//   [InternalError]()
	//   [KeywordPlanAdGroupKeywordError]()
	//   [KeywordPlanCampaignError]()
	//   [KeywordPlanError]()
	//   [LabelError]()
	//   [ListOperationError]()
	//   [MediaUploadError]()
	//   [MutateError]()
	//   [NewResourceCreationError]()
	//   [NullError]()
	//   [OperationAccessDeniedError]()
	//   [PolicyFindingError]()
	//   [PolicyViolationError]()
	//   [QuotaError]()
	//   [RangeError]()
	//   [RequestError]()
	//   [ResourceCountLimitExceededError]()
	//   [SettingError]()
	//   [SharedSetError]()
	//   [SizeLimitError]()
	//   [StringFormatError]()
	//   [StringLengthError]()
	//   [UrlFieldError]()
	//   [UserListError]()
	//   [YoutubeVideoRegistrationError]()
	Mutate(ctx context.Context, in *MutateRequest, opts ...grpc.CallOption) (*MutateResponse, error)

	// GetCustomerId login-customer-id
	GetCustomerId(ctx context.Context) (string, error)
}

type (
	SearchStreamRequest  = services.SearchGoogleAdsStreamRequest
	SearchStreamResponse = services.SearchGoogleAdsStreamResponse
	SearchRow            = services.GoogleAdsRow
	MutateRequest        = services.MutateGoogleAdsRequest
	MutateResponse       = services.MutateGoogleAdsResponse
	MutateOperation      = services.MutateOperation
	SummaryRowSetting    = enums.SummaryRowSettingEnum_SummaryRowSetting
	ResponseContentType  = enums.ResponseContentTypeEnum_ResponseContentType

	mutateCampaignOperation       = services.MutateOperation_CampaignOperation
	campaignOperation             = services.CampaignOperation
	campaignUpdate                = services.CampaignOperation_Update
	mutateCampaignBudgetOperation = services.MutateOperation_CampaignBudgetOperation
	campaignBudgetOperation       = services.CampaignBudgetOperation
	campaignBudgetUpdate          = services.CampaignBudgetOperation_Update
)

const (
	// SummaryRowUnspecified Not specified.
	SummaryRowUnspecified SummaryRowSetting = enums.SummaryRowSettingEnum_UNSPECIFIED
	// SummaryRowUnknown Represent unknown values of return summary row.
	SummaryRowUnknown SummaryRowSetting = enums.SummaryRowSettingEnum_UNKNOWN
	// SummaryRowNo Do not return summary row.
	SummaryRowNo SummaryRowSetting = enums.SummaryRowSettingEnum_NO_SUMMARY_ROW
	// SummaryRowYes Return summary row along with results. The summary row will be returned
	// in the last batch alone (last batch will contain no results).
	SummaryRowYes SummaryRowSetting = enums.SummaryRowSettingEnum_SUMMARY_ROW_WITH_RESULTS
	// SummaryRowOnly Return summary row only and return no results.
	SummaryRowOnly SummaryRowSetting = enums.SummaryRowSettingEnum_SUMMARY_ROW_ONLY

	// ResponseContentTypeUnspecified Not specified. Will return the resource name only in the response.
	ResponseContentTypeUnspecified ResponseContentType = enums.ResponseContentTypeEnum_UNSPECIFIED
	// ResponseContentTypeName The mutate response will be the resource name.
	ResponseContentTypeName ResponseContentType = enums.ResponseContentTypeEnum_RESOURCE_NAME_ONLY
	// ResponseContentTypeResource The mutate response will be the resource name and the resource with
	// all mutable fields.
	ResponseContentTypeResource ResponseContentType = enums.ResponseContentTypeEnum_MUTABLE_RESOURCE
)

type ClientConnection interface {
	grpc.ClientConnInterface
	GetCustomerId(ctx context.Context) (string, error)
}

func NewGoogleAdsServiceClient(con ClientConnection) *GoogleAdsServiceClient {
	return &GoogleAdsServiceClient{services.NewGoogleAdsServiceClient(con), con}
}

type GoogleAdsServiceClient struct {
	client services.GoogleAdsServiceClient
	conn   customerId
}

const NoCustomerId = "-"

func (g *GoogleAdsServiceClient) SearchStream(ctx context.Context, in *SearchStreamRequest, opts ...grpc.CallOption) (SearchStream, error) {
	if strings.HasPrefix(in.Query, invalidQuery) {
		return nil, errors.New(in.Query)
	}

	var err error
	in.CustomerId, err = g.getCustomerId(ctx, in.CustomerId)
	if err != nil {
		return nil, err
	}

	ctx, cancel := context.WithCancel(ctx)
	stream, err := g.client.SearchStream(ctx, in, opts...)
	if err != nil {
		cancel()
		return nil, err
	}
	return &searchStream{
		stream: stream,
		cancel: cancel,
	}, nil
}

func (g *GoogleAdsServiceClient) Mutate(ctx context.Context, in *MutateRequest, opts ...grpc.CallOption) (*MutateResponse, error) {
	var err error
	in.CustomerId, err = g.getCustomerId(ctx, in.CustomerId)
	if err != nil {
		return nil, err
	}
	return g.client.Mutate(ctx, in, opts...)
}

func (g *GoogleAdsServiceClient) getCustomerId(ctx context.Context, customerId string) (result string, err error) {
	if customerId == "" {
		result, err = g.GetCustomerId(ctx)
		if err != nil {
			return customerId, err
		}
	} else if customerId == NoCustomerId {
		result = ""
	} else {
		result = customerId
	}
	return
}

func (g *GoogleAdsServiceClient) GetCustomerId(ctx context.Context) (string, error) {
	return g.conn.GetCustomerId(ctx)
}

func CampaignUpdate(c *Campaign, updateMask ...*fieldmaskpb.FieldMask) *MutateOperation {
	return &MutateOperation{
		Operation: &mutateCampaignOperation{
			CampaignOperation: &campaignOperation{
				UpdateMask: fieldMask(c, updateMask...),
				Operation: &campaignUpdate{
					Update: c,
				},
			},
		},
	}
}

func CampaignBudgetUpdate(b *CampaignBudget, updateMask ...*fieldmaskpb.FieldMask) *MutateOperation {
	return &MutateOperation{
		Operation: &mutateCampaignBudgetOperation{
			CampaignBudgetOperation: &campaignBudgetOperation{
				UpdateMask: fieldMask(b, updateMask...),
				Operation: &campaignBudgetUpdate{
					Update: b,
				},
			},
		},
	}
}
