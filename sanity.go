package google_ads

import (
	"fmt"
	"path"
	"reflect"
)

func init() {
	tp := reflect.TypeOf(GoogleAdsServiceClient{})
	pkg := tp.PkgPath()
	_, pkgVersion := path.Split(pkg)
	if pkgVersion != version {
		panic(fmt.Errorf("invalid google_ads_api versions %s in package %s", version, pkg))
	}
}
