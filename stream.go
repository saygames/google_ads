package google_ads

import (
	"errors"
	"io"

	"google.golang.org/genproto/googleapis/ads/googleads/v18/services"
)

type SearchStream interface {
	Next() bool
	Get() (*SearchStreamResponse, error)
	Close() error
}

type searchStream struct {
	stream   services.GoogleAdsService_SearchStreamClient
	cancel   func()
	lastResp *SearchStreamResponse
	lastErr  error
}

var (
	errStreamClosed = errors.New("stream is closed")
	errNilStream    = errors.New("stream is nil")
)

func (s *searchStream) Get() (*SearchStreamResponse, error) {
	if s == nil {
		return nil, errNilStream
	}
	return s.lastResp, s.lastErr
}

func (s *searchStream) Next() bool {
	if s == nil {
		return false
	}
	if s.stream == nil {
		s.lastResp, s.lastErr = nil, errStreamClosed
		return false
	}
	hadErr := s.lastErr != nil
	s.lastResp, s.lastErr = s.stream.Recv()
	if s.lastErr == io.EOF {
		return false
	}
	if s.lastErr == nil {
		return true
	}
	if hadErr {
		// lastErr was reported on previous iteration, so break the loop
		return false
	} else {
		// new lastErr. Will be reported in Get
		return true
	}
}

func (s *searchStream) Close() error {
	if s == nil {
		return nil
	}
	s.stream = nil
	s.cancel()
	return nil
}
