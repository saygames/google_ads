package google_ads

import (
	"errors"
	"fmt"
	"reflect"
	"strconv"
	"strings"
	"time"
)

type QueryBuilder struct {
	Fields  []string
	From    string
	Where   []Predicate
	OrderBy []string
	Limit   int
}

type Predicate struct {
	Field    string
	Operator string
	Value    interface{}
}

type (
	Literal  string
	Date     time.Time
	DateTime time.Time
)

var (
	errNoFields      = errors.New("query should have at least one field")
	errNoTable       = errors.New("QueryBuilder.From should be set")
	errPreNoField    = errors.New("Predicate.Field should be set")
	errPreNoOp       = errors.New("Predicate.Operator should be set")
	errPreNilValue   = errors.New("Predicate.Value should be nil")
	errType          = errors.New("type is not supported")
	errBetweenLen    = errors.New("operator BETWEEN requires two elements")
	errEmptyLiteral  = errors.New("literal can't be empty")
	errUnwrappedTime = errors.New("time.Time should be explicit wrapped as google_ads.Date or google_ads.DateTime")
)

func errOpType(op, tp string) error {
	return fmt.Errorf("operator %s value type should be %s", op, tp)
}

func errOpRequires(op string) error {
	return fmt.Errorf("operator %s requires non-zero value", op)
}

func (b QueryBuilder) Build() (string, error) {
	if len(b.Fields) == 0 {
		return "", errNoFields
	}
	if b.From == "" {
		return "", errNoTable
	}
	var builder strings.Builder
	builder.WriteString("SELECT ")
	builder.WriteString(strings.Join(b.Fields, ", "))
	builder.WriteString(" FROM ")
	builder.WriteString(b.From)
	if len(b.Where) > 0 {
		builder.WriteString(" WHERE ")
		for i, pred := range b.Where {
			cond, err := pred.Build()
			if err != nil {
				return "", err
			}
			builder.WriteString(cond)
			if i < len(b.Where)-1 {
				builder.WriteString(" AND ")
			}
		}
	}
	if len(b.OrderBy) > 0 {
		builder.WriteString(" ORDER BY ")
		builder.WriteString(strings.Join(b.OrderBy, ", "))
	}
	if b.Limit > 0 {
		builder.WriteString(" LIMIT " + strconv.Itoa(b.Limit))
	}
	return builder.String(), nil
}

const invalidQuery = "invalid query: "

func (b QueryBuilder) String() string {
	q, e := b.Build()
	if e != nil {
		return invalidQuery + e.Error()
	}
	return q
}

func (p Predicate) Build() (string, error) {
	if p.Field == "" {
		return "", errPreNoField
	}
	if p.Operator == "" {
		return "", errPreNoOp
	}
	v, e := renderQueryValue(p.Operator, p.Value)
	if e != nil {
		return "", e
	}
	return p.Field + " " + p.Operator + v, nil
}

func renderQueryValue(op string, val interface{}) (string, error) {
	v, e := _renderQueryValue(op, val)
	if v != "" {
		v = " " + v
	}
	return v, e
}

func _renderQueryValue(op string, val interface{}) (string, error) {
	switch strings.ToUpper(op) {
	case "CONTAINS ANY", "CONTAINS ALL", "CONTAINS NONE":
		return "", errors.New("TODO: no reference for this operator")
	case "IN", "NOT IN":
		return renderQueryList(op, val)
	case "IS NULL", "IS NOT NULL":
		if val != nil {
			return "", fmt.Errorf("%s: %w", op, errPreNilValue)
		}
		return "", nil
	case "DURING":
		fn, ok := val.(string)
		if !ok {
			return "", errOpType(op, "string")
		}
		if fn == "" {
			return "", errOpRequires(op)
		}
		return fn, nil
	case "BETWEEN":
		return renderQueryBetween(val)
	default:
		return renderQuerySingle(val)
	}
}

func renderQuerySingle(i interface{}) (string, error) {
	switch v := i.(type) {
	case Literal:
		if v == "" {
			return "", errEmptyLiteral
		}
		return string(v), nil
	case string:
		return renderQueryString(v)
	case DateTime:
		tm := time.Time(v)
		return `'` + tm.Format("2006-01-02 15:04:05") + `'`, nil
	case Date:
		tm := time.Time(v)
		return `'` + tm.Format("2006-01-02") + `'`, nil
	case time.Time:
		return "", errUnwrappedTime
	case fmt.Stringer:
		return renderQueryString(v.String())
	case int:
		return strconv.Itoa(v), nil
	case int64:
		return strconv.FormatInt(v, 10), nil
	case int32:
		return strconv.FormatInt(int64(v), 10), nil
	case int16:
		return strconv.FormatInt(int64(v), 10), nil
	case int8:
		return strconv.FormatInt(int64(v), 10), nil
	case uint:
		return strconv.FormatUint(uint64(v), 10), nil
	case uint64:
		return strconv.FormatUint(v, 10), nil
	case uint32:
		return strconv.FormatUint(uint64(v), 10), nil
	case uint16:
		return strconv.FormatUint(uint64(v), 10), nil
	case uint8:
		return strconv.FormatUint(uint64(v), 10), nil
	case float64:
		return strconv.FormatFloat(v, 'f', -1, 64), nil
	case float32:
		return strconv.FormatFloat(float64(v), 'f', -1, 64), nil
	case bool:
		return "", errors.New("TODO: bool literal")
	default:
		return "", fmt.Errorf("%T: %w", i, errType)
	}
}

func renderQueryList(op string, i interface{}) (string, error) {
	val := reflect.ValueOf(i)
	if !val.IsValid() && !val.IsNil() {
		return "", errOpRequires(op)
	}
	if val.Kind() != reflect.Array && val.Kind() != reflect.Slice {
		v, e := renderQuerySingle(i)
		if e != nil {
			return "", e
		}
		return "(" + v + ")", nil
	}
	var builder strings.Builder
	builder.WriteRune('(')
	ln := val.Len()
	for i := 0; i < ln; i++ {
		el := val.Index(i).Interface()
		v, e := renderQuerySingle(el)
		if e != nil {
			return "", e
		}
		builder.WriteString(v)
		if i < ln-1 {
			builder.WriteString(", ")
		}
	}
	builder.WriteRune(')')
	return builder.String(), nil
}

func renderQueryBetween(i interface{}) (string, error) {
	val := reflect.ValueOf(i)
	if !val.IsValid() && !val.IsNil() {
		return "", errOpRequires("BETWEEN")
	}
	if val.Kind() != reflect.Array && val.Kind() != reflect.Slice {
		return "", errOpType("BETWEEN", "slice")
	}
	if val.Len() != 2 {
		return "", errBetweenLen
	}
	left, err := renderQuerySingle(val.Index(0).Interface())
	if err != nil {
		return "", err
	}
	right, err := renderQuerySingle(val.Index(1).Interface())
	if err != nil {
		return "", err
	}
	return left + " AND " + right, nil
}

func renderQueryString(v string) (string, error) {
	if !strings.Contains(v, `'`) {
		return `'` + v + `'`, nil
	} else if !strings.Contains(v, `"`) {
		return `"` + v + `"`, nil
	} else {
		return "", errors.New("TODO: implement escape")
	}
}
