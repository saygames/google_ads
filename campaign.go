package google_ads

import (
	"google.golang.org/genproto/googleapis/ads/googleads/v18/common"
	"google.golang.org/genproto/googleapis/ads/googleads/v18/enums"
	"google.golang.org/genproto/googleapis/ads/googleads/v18/resources"
)

type (
	BiddingStrategyGoalType = enums.AppCampaignBiddingStrategyGoalTypeEnum_AppCampaignBiddingStrategyGoalType
	Campaign                = resources.Campaign
	CampaignTargetCpa       = resources.Campaign_TargetCpa
	CampaignTargetRoas      = resources.Campaign_TargetRoas
	TargetCpa               = common.TargetCpa
	TargetRaos              = common.TargetRoas
)

const (
	// BiddingStrategyGoalUnspecified Not specified.
	BiddingStrategyGoalUnspecified BiddingStrategyGoalType = enums.AppCampaignBiddingStrategyGoalTypeEnum_UNSPECIFIED
	// BiddingStrategyGoalUnknown Used for return value only. Represents value unknown in this version.
	BiddingStrategyGoalUnknown BiddingStrategyGoalType = enums.AppCampaignBiddingStrategyGoalTypeEnum_UNKNOWN
	// BiddingStrategyGoalCpi Aim to maximize the number of app installs. The cpa bid is the
	// target cost per install.
	BiddingStrategyGoalCpi BiddingStrategyGoalType = enums.AppCampaignBiddingStrategyGoalTypeEnum_OPTIMIZE_INSTALLS_TARGET_INSTALL_COST
	// BiddingStrategyGoalOptimizeInAppConversionsTargetInstallCost Aim to maximize the long term number of selected in-app conversions from
	// app installs. The cpa bid is the target cost per install.
	BiddingStrategyGoalOptimizeInAppConversionsTargetInstallCost BiddingStrategyGoalType = enums.AppCampaignBiddingStrategyGoalTypeEnum_OPTIMIZE_IN_APP_CONVERSIONS_TARGET_INSTALL_COST
	// BiddingStrategyGoalCpa Aim to maximize the long term number of selected in-app conversions from
	// app installs. The cpa bid is the target cost per in-app conversion. Note
	// that the actual cpa may seem higher than the target cpa at first, since
	// the long term conversions haven't happened yet.
	BiddingStrategyGoalCpa BiddingStrategyGoalType = enums.AppCampaignBiddingStrategyGoalTypeEnum_OPTIMIZE_IN_APP_CONVERSIONS_TARGET_CONVERSION_COST
	// BiddingStrategyGoalRoas Aim to maximize all conversions' value, i.e. install + selected in-app
	// conversions while achieving or exceeding target return on advertising
	// spend.
	BiddingStrategyGoalRoas BiddingStrategyGoalType = enums.AppCampaignBiddingStrategyGoalTypeEnum_OPTIMIZE_RETURN_ON_ADVERTISING_SPEND
	// BiddingStrategyGoalOptimizePreRegistrationConversionVolume Aim to maximize the pre-registration of the app.
	BiddingStrategyGoalOptimizePreRegistrationConversionVolume BiddingStrategyGoalType = enums.AppCampaignBiddingStrategyGoalTypeEnum_OPTIMIZE_PRE_REGISTRATION_CONVERSION_VOLUME
	// BiddingStrategyGoalOptimizeInstallsWithoutTargetInstallCost Aim to maximize installation of the app without target cost-per-install.
	BiddingStrategyGoalOptimizeInstallsWithoutTargetInstallCost BiddingStrategyGoalType = enums.AppCampaignBiddingStrategyGoalTypeEnum_OPTIMIZE_INSTALLS_WITHOUT_TARGET_INSTALL_COST
)

func CampaignResourceName(customerId string, campaignId string) string {
	return "customers/" + customerId + "/campaigns/" + campaignId
}
