module bitbucket.org/saygames/google_ads/v18

go 1.22.7

require (
	github.com/TelpeNight/oauthctx v0.3.1
	golang.org/x/oauth2 v0.24.0
	google.golang.org/genproto/googleapis/ads/googleads/v18 v18.0.1
	google.golang.org/grpc v1.69.0
	google.golang.org/protobuf v1.35.2
)

require (
	cloud.google.com/go/compute/metadata v0.5.2 // indirect
	cloud.google.com/go/longrunning v0.6.3 // indirect
	github.com/golang/protobuf v1.5.4 // indirect
	golang.org/x/net v0.30.0 // indirect
	golang.org/x/sync v0.8.0 // indirect
	golang.org/x/sys v0.26.0 // indirect
	golang.org/x/text v0.19.0 // indirect
	google.golang.org/genproto v0.0.0-20241118233622-e639e219e697 // indirect
	google.golang.org/genproto/googleapis/api v0.0.0-20241118233622-e639e219e697 // indirect
	google.golang.org/genproto/googleapis/rpc v0.0.0-20241118233622-e639e219e697 // indirect
)

replace google.golang.org/genproto/googleapis/ads/googleads/v18 => bitbucket.org/saygames/google_ads_api/v18 v18.0.1
