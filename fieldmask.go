package google_ads

import (
	"strings"

	"google.golang.org/protobuf/proto"
	"google.golang.org/protobuf/reflect/protoreflect"
	"google.golang.org/protobuf/types/known/fieldmaskpb"
)

func fieldMask(msg proto.Message, optional ...*fieldmaskpb.FieldMask) *fieldmaskpb.FieldMask {
	if len(optional) > 0 && optional[0] != nil {
		return optional[0]
	}
	var paths []string
	msg.ProtoReflect().Range(func(fd protoreflect.FieldDescriptor, v protoreflect.Value) bool {
		paths = append(paths, fieldMaskRec(fd, v, nil)...)
		return true
	})
	fm, err := fieldmaskpb.New(msg, paths...)
	if err != nil {
		panic(err)
	}
	return fm
}

func skipFields(name protoreflect.Name) bool {
	switch name {
	case "", "resource_name":
		return true
	default:
		return false
	}
}

func fieldMaskRec(fd protoreflect.FieldDescriptor, v protoreflect.Value, path []string) []string {
	if skipFields(fd.Name()) {
		return nil
	}
	path = append(path, string(fd.Name()))
	if fd.Kind() == protoreflect.MessageKind {
		var results []string
		v.Message().Range(func(fd protoreflect.FieldDescriptor, v protoreflect.Value) bool {
			results = append(results, fieldMaskRec(fd, v, path)...)
			return true
		})
		return results
	} else {
		return []string{strings.Join(path, ".")}
	}
}
